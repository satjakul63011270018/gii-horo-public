module.exports = {
  apps: [
    {
      name: 'gii-horo-public',
      script: 'yarn',
      args: 'start',
      interpreter: 'none',
      max_restarts: 50,
      restart_delay: 3000,
    },
  ],
};
