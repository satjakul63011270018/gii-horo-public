import { theme } from './theme/themeVariables';

const config = {
  menuCollapse: false,
  topMenu: true,
  rtl: false,
  mainTemplate: 'lightMode',
  loggedIn: false,
  theme,
};

export default config;
