import { combineReducers } from 'redux';
import ChangeLayoutMode from './themeLayout/reducers';

const rootReducers = combineReducers({
  ChangeLayoutMode,
});

export default rootReducers;
