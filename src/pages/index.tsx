import Main from '@/layout/main-layout/main';
import React from 'react';

function BlankPage() {
  return (
    <Main>
      <div className="contanet">
        <section className="hero-banner">
          <div className="container h-100">
            <div className="row h-100">
              <div className="col-6 h-100">
                <div className="d-flex justify-center align-items-end h-100">
                  <a href="" className="btn btn-register-seer">
                    สมัครเป็นหมอดู
                  </a>
                </div>
              </div>
              <div className="col-6 h-100">
                <div className="d-flex justify-center align-items-center h-100">
                  <div className="box-text">
                    <h1>Astrology Readings & Shamanic Healing</h1>
                    <p className="pt-5">
                      Blend the timeless wisdom of classical astrology with shamanic energy work and modern cognitive
                      psychology
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="hero-banner">
          <div className="container h-100">
            <div className="row h-100">
              <div className="col-6 h-100">
                <div className="d-flex justify-center align-items-center h-100">
                  {/* <img src="/img/home/Horoscope.png" alt="" /> */}
                </div>
              </div>
              <div className="col-6 h-100">
                <div className="d-flex justify-center align-items-center h-100">
                  <div className="box-text">
                    <h1>โต๊ะดูดวง</h1>
                    <ul>
                      <li className="mt-3">- เหมือนจริง</li>
                      <li className="mt-1">- จับไพ่จริง</li>
                      <li className="mt-1">- realtime</li>
                      <li className="mt-1">- video call กับหมอดู</li>
                      <li className="mt-1">- เหมือนนั่งดูกันตรงหน้า</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </Main>
  );
}

export default BlankPage;
