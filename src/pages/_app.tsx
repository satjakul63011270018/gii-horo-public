import '@/styles/globals.css';
import '@/styles/styles.css';
import { useRouter } from 'next/router';
import type { AppProps } from 'next/app';

import { Provider } from 'react-redux';
import AdminLayout from './adminLayout';
import { wrapper, store } from '../redux/store';
import '../i18n/config';

function App({ Component, pageProps }: AppProps) {
  const router = useRouter();

  const renderLayout = () => {
    return (
      // <AdminLayout>
      <Component {...pageProps} />
      // </AdminLayout>
    );
  };

  return (
    <>
      <Provider store={store}>{renderLayout()}</Provider>
    </>
  );
}

export default wrapper.withRedux(App);
