import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { Layout } from 'antd';
import { useSelector } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import Footer from '@/layout/footer';
import Sidebar from '@/layout/sidebar';
import HeaderTop from '@/layout/header';

const { Content } = Layout;

import config from '@/config/config';
const { theme } = config;

const AdminLayout = ({ children }: { children: React.ReactNode }) => {
  const {
    topMenu,
    menuCollapse: collapsed,
    rtlData: rtl,
    mode: mainContent,
  } = useSelector((state: any) => state.ChangeLayoutMode);

  if (mainContent === 'darkMode') {
    document.body.classList.add('dark');
    document.body.classList.add('dark');
  }

  if (rtl) {
    const html: any = document.querySelector('html');
    html.setAttribute('dir', 'rtl');
  }

  return (
    <ThemeProvider theme={theme}>
      <HeaderTop />

      <div className="flex flex-row gap-5 mt-[72px]">
        <Sidebar />

        <Layout
          className={`max-w-full duration-[300ms] ${
            !topMenu ? `xl:ps-0 ease-[ease] ${collapsed ? 'ps-[80px]' : 'ps-[280px] delay-[150ms]'}` : ''
          }`}
        >
          <Content>
            {children}
            <Footer />
          </Content>
        </Layout>
      </div>
    </ThemeProvider>
  );
};

export default AdminLayout;
