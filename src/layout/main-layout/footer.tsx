import React from 'react';

function Footer() {
  return (
    <footer>
      <div className="container pt-5">
        <div className="row">
          <div className="col-3">
            <a className="navbar-brand" href="#">
              <img src="/img/home/GIIHORO.svg" className="nav-logo" alt="" />
            </a>
          </div>
          <div className="col-4">
            <div className="d-flex justify-content-center">
              <ul>
                <li>Menu</li>
                <li className="mt-3">หน้าแรก</li>
                <li className="mt-3">ราคา</li>
              </ul>
            </div>
          </div>
          <div className="col-4">
            <div className="d-flex justify-content-center">
              <ul>
                <li>Content</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
