import { ReactNode, Fragment } from 'react';
import Footer from './footer';
import Herder from './header';
import React from 'react';

interface Props {
  children?: ReactNode;
}
function Main({ children }: Props) {
  return (
    <Fragment>
      <Herder />
      {children}
      <Footer />
    </Fragment>
  );
}

export default Main;
