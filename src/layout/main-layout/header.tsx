import React, { useState, useEffect } from 'react';

function Header() {
  const [scrolled, setScrolled] = useState(false);

  const handleScroll = () => {
    const offset = window.scrollY;
    if (offset > 10) {
      setScrolled(true);
    } else {
      setScrolled(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <nav className={`navbar navbar-expand-lg navbar-horo ${scrolled ? 'navbar-active' : ''}`}>
      <div className="container">
        <a className="navbar-brand" href="#">
          <img src="/img/home/GIIHORO.svg" className="nav-logo" alt="" />
        </a>
        <div className="navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item ms-4 me-5">
              <a className="nav-link font-18 active text-white" aria-current="page" href="#">
                หน้าแรก
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link font-18 text-white" href="#">
                ราคา
              </a>
            </li>
          </ul>

          <div className="ms-auto">
            <a href="" className="btn-register btn me-5">
              สมัครเป็นหมอดู
            </a>
            <a href="" className="btn-login btn">
              เข้าสู่ระบบ
            </a>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Header;
